#!/bin/bash
# Creates a patch from current branch to master.
# git format-patch master --stdout > $1.patch
# Another solution :
git diff --patch master --output $1.patch --binary --full-index --no-color 

