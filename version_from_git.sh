r=`git describe`
r_code=$?
echo $r_code
version=""
if [ $r_code = 128 ] ; then
# case where no tag is available
    version=`git describe --all --long --dirty`
else
    version=`git describe --long --dirty`
fi

echo $version > VERSION